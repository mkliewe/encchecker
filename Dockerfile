FROM golang:1.24-alpine AS builder

WORKDIR /build

COPY . ./
RUN go mod download

# Set necessarry environment vairables and compile the app
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -v -mod=vendor -ldflags="-s -w" -o encchecker .

FROM scratch

LABEL org.opencontainers.image.authors="christian@roessner.email"
LABEL com.roessner-network-solutions.vendor="Rößner-Network-Solutions"
LABEL version="@@gittag@@-@@gitcommit@@"
LABEL description="Check mail header, if they conform to ASCII"

WORKDIR /usr/app

# Copy binary to destination image
COPY --from=builder ["/build/encchecker", "./"]

EXPOSE 4649

CMD ["/usr/app/encchecker", "-verbose"]