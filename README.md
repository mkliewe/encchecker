# encchecker

## About

Postfix-Submission Milter that detects illegal UTF-8 encoded headers (headers have to be 7bit only)

Postfix has a built-in feature named "strict_7bit_headers", but it does not have a "warn/log-only" feature. If you want to
investigate how many and which mails would be affected by "strict_7bit_headers = yes", you can run `encchecker` in "logonly" mode.

## Features

* Checks each header of an email for invalid 8bit characters
* Can be run in "reject" or "logonly" mode

# Table of contents

1. [Install](#install)
    * [Postfix integration](#postfix-integration)
    * [Preparing a docker image](#preparing-a-docker-image)
    * [Build from source](#build-from-source)
    * [Server options](#server-options)

# Install

## Postfix integration

The service is configured in Postfix like this...

```
smtpd_milters={ inet:127.0.0.1:4649, connect_timeout=10s, command_timeout=10s, content_timeout=10s, default_action=accept }
```

... if you use the docker-compose.yml file as provided.

Back to [table of contents](#table-of-contents)

## Preparing a docker image

The simplest way to use the program is by using a docker image.

```shell
cd /path/to/Dockerfile
docker build -t encchecker:latest .
```

Back to [table of contents](#table-of-contents)

## Build from source

### Requirements

To build encchecker from source, you need at least Golang version 1.24 or above.

### Compile

```shell
cd /path/to/sourcecode
go build -mod=vendor -o encchecker .
```

You can find an example systemd unit file in the contrib folder.

Back to [table of contents](#table-of-contents)

## Server options

```shell
/usr/local/bin/encchecker --help
```

produces the following output:

```
Usage of /usr/local/bin/encchecker:
  -addr string
        Bind to address or unix domain socket (default "127.0.0.1:4649")
  -logonly
        Only log results without rejecting emails
  -proto string
        Protocol family (unix or tcp) (default "tcp")
  -reply string
        Custom response message (default "554 5.6.0 Message blocked due to illegal UTF-8 header encoding")
  -verbose
        Enable verbose output
```

Back to [table of contents](#table-of-contents)

Hope you enjoy :-)