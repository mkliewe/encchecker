/* bogomilter is a milter service for postfix */
package main

import (
	"flag"
	"github.com/phalaaxx/milter"
	"log"
	"net"
	"net/textproto"
	"os"
)

type SomeMilter struct {
	milter.Milter
}

func (s *SomeMilter) Connect(host string, family string, port uint16, addr net.IP, m *milter.Modifier) (milter.Response, error) {
	log.Println("connect:", host, family, port, addr, m)
	return milter.RespContinue, nil
}

func (s *SomeMilter) Helo(name string, m *milter.Modifier) (milter.Response, error) {
	log.Println("helo:", name, m)
	return milter.RespContinue, nil
}

func (s *SomeMilter) MailFrom(from string, m *milter.Modifier) (milter.Response, error) {
	log.Println("mail from:", from, m)
	return milter.RespContinue, nil
}

func (s *SomeMilter) RcptTo(rcptTo string, m *milter.Modifier) (milter.Response, error) {
	log.Println("rcpt to:", rcptTo, m)
	return milter.RespContinue, nil
}

func (s SomeMilter) Header(name string, value string, m *milter.Modifier) (milter.Response, error) {
	log.Println("header:", name, value, m)
	return milter.RespContinue, nil
}

func (s *SomeMilter) Headers(headers textproto.MIMEHeader, m *milter.Modifier) (milter.Response, error) {
	log.Println("headers:", headers, m)
	return milter.RespContinue, nil
}

func (s SomeMilter) BodyChunk(chunk []byte, m *milter.Modifier) (milter.Response, error) {
	_ = chunk
	log.Println("body chunk:", m)
	return milter.RespContinue, nil
}

func (s *SomeMilter) Body(m *milter.Modifier) (milter.Response, error) {
	log.Println("body:", m)
	return milter.RespContinue, nil
}

func main() {
	// parse commandline arguments
	var protocol, address string
	flag.StringVar(&protocol,
		"proto",
		"tcp",
		"Protocol family (unix or tcp)")
	flag.StringVar(&address,
		"addr",
		"[::]:4648",
		"Bind to address or unix domain socket")
	flag.Parse()

	// Make sure the specified protocol is either unix or tcp
	if protocol != "unix" && protocol != "tcp" {
		log.Fatal("invalid protocol name")
	}

	if protocol == "unix" {
		// ignore os.Remove errors
		err := os.Remove(address)
		if err != nil {
			log.Fatalln(err)
		}
	}

	socket, err := net.Listen(protocol, address)
	if err != nil {
		log.Fatal(err)
	}
	//goland:noinspection GoUnhandledErrorResult
	defer socket.Close()

	if protocol == "unix" {
		if err := os.Chmod(address, 0660); err != nil {
			log.Fatal(err)
		}
		//goland:noinspection GoUnhandledErrorResult
		defer os.Remove(address)
	}

	var NoAction milter.OptAction = 0x0
	var NoProtocol milter.OptProtocol = 0x0

	// Declare milter init function
	init := func() (milter.Milter, milter.OptAction, milter.OptProtocol) {
		return &SomeMilter{}, NoAction, NoProtocol
	}
	// Start server
	if err := milter.RunServer(socket, init); err != nil {
		log.Fatal(err)
	}
}
